import {
  warningColor,
  primaryColor,
  dangerColor,
  successColor,
  infoColor,
  roseColor,
  grayColor,
  defaultFont
} from "resources/style/base.jsx";

const tableStyle = theme => ({
  warningTableHeader: {
    color: warningColor
  },
  primaryTableHeader: {
    color: primaryColor
  },
  dangerTableHeader: {
    color: dangerColor
  },
  successTableHeader: {
    color: successColor
  },
  infoTableHeader: {
    color: infoColor
  },
  roseTableHeader: {
    color: roseColor
  },
  grayTableHeader: {
    color: grayColor
  },
  table: {
    "&::-webkit-scrollbar":{
      width: "8px !important",
      height: "8px !important"
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "#ffffff"
    },
    "&::-webkit-scrollbar-thumb": {
      borderRadius: "0.5rem",
      backgroundColor: "#BEBEBE"
    },
    marginBottom: "0",
    width: "calc(100% - 8px)",
    maxWidth: "calc(100% - 8px)",
    height:"calc(100% - 8px)",
    display:"block",
    overflow:"auto",
    backgroundColor: "transparent",
    borderSpacing: "0",
    margin:"4px",
    borderCollapse: "collapse"
  },
  tableHeadCell: {
    color: "inherit",
    ...defaultFont,
    fontSize: "1em"
  },
  itemSelectstyles: {
    background:"#e1f2e1 !important"
  },
  tableCell: {
    ...defaultFont,
    lineHeight: "1.42857143",
    verticalAlign: "middle",
    height:"36px"
  },
  tableCellText:{
    marginTop:"auto",
    marginBottom:"auto",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 2,
    "-webkit-box-orient": "vertical"
  },
  tableResponsive: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  buttonHeaderline:{
    width:"1rem",
    height:"1rem"
  },
  buttonHeaderlineIcon:{
    fontSize:"1rem"
  },
  textField:{
    width:"4rem",
    margin :"0",
    height:"2rem"
  },
  MenuItem:{
    paddingRight:"0",
    fontSize: "0.75rem",
    paddingTop:" 0.25rem",
    paddingBottom: "0.25rem",
    paddingRight:"0.25rem"
  },
  menuItemButtom:{
    height: "1.5rem",
    width:"1.5rem",
    marginLeft:"1rem",
    color:"#ffffff",
    background:"#1E90FF !important"
  },
  menuItemButtomSearch:{
    height: "1.5rem",
    width:"1.5rem",
    color:"#ffffff !important",
    background:"#1E90FF !important"
  }
});

export default tableStyle;
