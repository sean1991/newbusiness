import {
  drawerWidth,
  transition,
  container
} from "resources/style/base.jsx";

const appStyle = theme => ({
  wrapper: {
    position: "relative",
    top: "0",
    height: "100vh"
  },
  showDraver:{
    position:"absolute",
    zIndex:"999"
  },
  ManuListSetup:{
    height:"48px",
    padding:"0",
    background:"#2F4F4F",
    borderTop:"#BEBEBE 1px solid"
  },
  ManuListGH:{
    height:`calc(100% - 48px)`,
    overflow: "auto",
    padding:"0"
  },
  ManuList:{
    background:"#5B5B5B",
    overflow: "hidden",
    position: "relative",
    float: "left",
    ...transition,
    maxHeight: "100%",
    width: "100px",
    height:"100%",
    overflowScrolling: "touch"
  },
  Item:{
    padding:"12px 0",
    display: "block",
    background:"#5B5B5B",
    borderTop: "#BEBEBE 1px solid"
  },
  itemSelected:{
    padding:"12px 0",
    display: "block",
    color:"#ffffff",
    background:"#212121",
    borderTop: "#BEBEBE 1px solid"
  },
  Button:{
    display: "flex",
    color:"#ffffff",
    margin: "auto"
  },
  ButtonSetup:{
    display: "flex",
    margin: "auto",
    height:"76px"
  },
  Text:{
    padding:"0",
    color:"#ffffff",
    textAlign:"center",
    display:"block"
  },
  mainPanel: {
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - 360px)`
    },
    overflow: "auto",
    position: "relative",
    float: "right",
    ...transition,
    maxHeight: "100%",
    width: "100%",
    overflowScrolling: "touch"
  },
  mainPanelFull:{
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - 100px)`
    },
    overflow: "auto",
    position: "relative",
    float: "right",
    ...transition,
    maxHeight: "100%",
    width: "100%",
    overflowScrolling: "touch"
  },
  content: {
    marginTop: "70px",
    padding: "30px 15px",
    minHeight: "calc(100vh - 123px)"
  },
  busyContent:{
    position:"absolute",
    height:"98%",
    width:"99%",
    opacity:"0.5",
    zIndex:"999",
    background:"#000000",
    display:"flex"
  },
  progress:{
    zIndex:"1000",
    color:"blue",
    margin:"auto"
  },
  container,
  map: {
    marginTop: "70px"
  }
});

export default appStyle;
