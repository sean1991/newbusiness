import {
  drawerWidth,
  transition,
  container
} from "resources/style/base.jsx";

const appStyle = theme => ({
  wrapper: {
    position: "relative",
    top: "0",
    height: "100vh"
  },
  showDraver:{
    position:"absolute",
    zIndex:"999"
  },
  ManuList:{
    overflow: "auto",
    position: "relative",
    float: "left",
    ...transition,
    maxHeight: "100%",
    width: "100px",
    height:"100%",
    overflowScrolling: "touch"
  },
  mainPanel: {
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - 360px)`
    },
    overflow: "auto",
    position: "relative",
    float: "right",
    ...transition,
    maxHeight: "100%",
    width: "100%",
    overflowScrolling: "touch"
  },
  mainPanelFull:{
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - 100px)`
    },
    overflow: "auto",
    position: "relative",
    float: "right",
    ...transition,
    maxHeight: "100%",
    width: "100%",
    overflowScrolling: "touch"
  },
  content: {
    marginTop: "0px",
    padding: "0px 15px",
    minHeight: "calc(100vh - 123px)"
  },
  busyContent:{
    position:"absolute",
    height:"98%",
    width:"99%",
    opacity:"0.5",
    zIndex:"999",
    background:"#000000",
    display:"flex"
  },
  progress:{
    zIndex:"1000",
    color:"blue",
    margin:"auto"
  },
  container,
  map: {
    marginTop: "0px"
  }
});

export default appStyle;
