const baseLabel={
  width:"auto",
  margin:"6px",
  borderRadius:"400px",
  padding:"2px",
  boxShadow: "0 0 2px rgba(156, 39, 176, 0.2)"
};

const baseHeight = window.innerHeight - 100 + "px";
const ContentHeight = window.innerHeight - 182 + "px";

const appStyle = theme => ({
  cardLayoutH:{
    marginTop:"10px",
    height:"50px",
    width:"100%"
  },
  cardLayoutB:{
    marginTop:"20px",
    marginBottom:"10px",
    width:"100%",
    height:baseHeight
  },
  cardLayoutBL:{
    margin:"0",
    padding:"0",
    width:"80px",
    height:"100%",
    position:"absolute",
    top:"0",
    left:"0",
    borderRight:"#E8E8E8 1px solid"
  },
  cardLayoutBH:{
    margin:"0",
    padding:"0",
    width:`calc(100% - 80px)`,
    height:"80px",
    position:"absolute",
    top:"0",
    left:"80px"
  },
  cardLayoutBC:{
    margin:"0",
    padding:"0",
    width:`calc(100% - 80px)`,
    height:ContentHeight,
    position:"absolute",
    top:"82px",
    left:"80px"
  },
  cardLayoutHBody:{
    padding:"0",
    display:"flex"
  },
  textField:{
    margin: "auto 16px auto 0",
    width: "80%",
    padding: "0 0 0 16px"
  },
  headerButton:{
    margin:"auto 0 auto 16px"
  },
  ManuBody:{
    padding:"0"
  },
  Item:{
    padding:"10px 0",
    display: "block",
    // background:"#BEBEBE",
    borderBottom:"#E8E8E8 1px solid"
  },
  itemSelect:{
    padding:"10px 0",
    display: "block",
    background:"#BEBEBE",
    borderBottom:"#E8E8E8 1px solid"
  },
  Button:{
    display: "flex",
    margin: "auto"
  },
  Text:{
    padding:"0",
    textAlign:"center",
    "& .MuiTypography-subheading":{
      fontSize:"0.5rem"
    }
  },
  LabelBody:{
    padding:"0",
    height:"50%",
    display:"flex",
    borderBottom:"#E8E8E8 1px solid"
  },
  ManuListGH:{
    padding:"0"
  },
  LabelContent:{
    fontSize:"0.75rem",
    color:"#ffffff",
    padding:"2px 4px",
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    textShadow: "0 0 2px #000000",
    fontWeight: "500",
  },
  baseLabel:{
    cursor:"pointer",
    ...baseLabel
  }
});
export default appStyle;
