import React, { Component }  from 'react';
import ReactDOM from 'react-dom';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import zh from 'react-intl/locale-data/zh';
import messages_en from './i18n/properties_en.json';
import messages_zh from './i18n/properties_zh.json';
addLocaleData([...en, ...zh]);

const language = (navigator.languages && navigator.languages[0]) ||
                     navigator.language ||
                     navigator.userLanguage;

const languageWithoutRegionCode = language.toLowerCase().split(/[_-]+/)[0];

const messages = languageWithoutRegionCode === "zh" ? messages_zh : messages_en;

class Translate extends Component {
	constructor(props) {
        super(props);
    }

    render() {
    	return (
	    	<IntlProvider locale={ language } messages={ messages }>
	    		{this.props.Template}
	    	</IntlProvider>
	    );
    }
}

export default Translate;
