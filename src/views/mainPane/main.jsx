import React from "react";
import { connect } from 'react-redux';

// @material-ui/core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Button from '@material-ui/core/Button';
import Graph from "views/displayMode/Graph.jsx";
import Table from "views/displayMode/Table.jsx";
import TextDisplay from "views/displayMode/TextDisplay.jsx";


import { FormattedMessage } from 'react-intl';

import styles from "resources/style/layouts/mainPane.jsx";
import generalAc from "actions/generalAction.js";

class MainPane extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayMode: 'G',//L->table, T->text
      queryStr:"",
      selectNodeL: Object.keys(props.nodePro)[0]
    };
  }

  changDisplayMode(sMode){
    this.setState({
      displayMode:sMode
    })
  }

  onNodeLabelClick(node){
    this.setState({
      selectNodeL:node
    })
  }

  clearSearchStr(){
    this.setState({
      queryStr:""
    })
  }

  query(){
    if (this.props.nodeQuery && this.props.data) {
      this.props.nodeQuery(this.state.queryStr);
    }
  }

  searchStrChange(e){
    let sQueryStr = e.target.value;
    this.setState({
      queryStr:sQueryStr
    })
  }

  renderBody(){
    switch (this.state.displayMode) {
      case "L":
        return (
          <Table nodePro={this.props.nodePro && this.props.nodePro[this.state.selectNodeL]}></Table>
        )
      case "T":
        return (
          <TextDisplay ></TextDisplay>
        )
      case "G":
      default:
      return (
        <Graph handleNodeClick={this.props.handleNodeClick} graphData={this.props.data} edgePro={this.props.edgePro} nodePro={this.props.nodePro}></Graph>
      )

    }
  }
  render(){
    const { classes } = this.props;

    const nodeLabels = (
      <CardBody className={classes.LabelBody}>
        {
          this.props.nodePro && Object.keys(this.props.nodePro).map((pro,key)=>{
          return <div className={classes.baseLabel} onClick={this.onNodeLabelClick.bind(this,pro)} style={{background:this.props.nodePro[pro].color}}>
                   <span className={classes.LabelContent}>{pro}</span>
                 </div>})
        }
      </CardBody>
    );

    const edgeLabels = (
      <CardBody className={classes.LabelBody}>
        {
          this.props.edgePro && Object.keys(this.props.edgePro).map((pro,key)=>{
          return <div className={classes.baseLabel} style={{background:this.props.edgePro[pro].color}} >
                   <span className={classes.LabelContent}>{pro}</span>
                 </div>})
        }
      </CardBody>
    );

    return (
      <div>
        <Card className={classes.cardLayoutH}>
          <CardBody className={classes.cardLayoutHBody}>
            <TextField
              id="search"
              placeholder="Search String"
              multiline
              value={this.state.queryStr}
              onChange={this.searchStrChange.bind(this)}
              className={classes.textField}
              margin="normal"
              variant="outlined"
            />
          <Button variant="fab" className={classes.headerButton} onClick={this.clearSearchStr.bind(this)} mini={true}>
              <Icon>cancel</Icon>
            </Button>
            <Button variant="fab" className={classes.headerButton} onClick={this.query.bind(this)} mini={true}>
              <Icon>play_circle_outline</Icon>
            </Button>
          </CardBody>
        </Card>
        <Card className={classes.cardLayoutB}>
          <CardBody>
            <card className={classes.cardLayoutBL}>
              <CardBody className={classes.ManuBody}>
                <List component="nav"  className={classes.ManuListGH}>
                  <ListItem
                    button
                    className={this.state.displayMode === "G" ? classes.itemSelect:classes.Item}
                    onClick={this.changDisplayMode.bind(this,"G")}
                  >
                    <ListItemIcon className={classes.Button}>
                      <Icon>all_inclusive</Icon>
                    </ListItemIcon>
                    <ListItemText className={classes.Text} primary="Graph" />
                  </ListItem>
                  <ListItem
                    button
                    className={this.state.displayMode === "L" ? classes.itemSelect:classes.Item}
                    onClick={this.changDisplayMode.bind(this,"L")}
                  >
                    <ListItemIcon className={classes.Button}>
                      <Icon>grid_on</Icon>
                    </ListItemIcon>
                    <ListItemText className={classes.Text} primary="Table" />
                  </ListItem>
                  <ListItem
                    button
                    className={this.state.displayMode === "T" ? classes.itemSelect:classes.Item}
                    onClick={this.changDisplayMode.bind(this,"T")}
                  >
                    <ListItemIcon className={classes.Button}>
                      <Icon>translate</Icon>
                    </ListItemIcon>
                    <ListItemText className={classes.Text} primary="Text" />
                  </ListItem>
                </List>
              </CardBody>
            </card>
            <card className={classes.cardLayoutBH}>
              {nodeLabels}
              {edgeLabels}
            </card>
            <card className={classes.cardLayoutBC}>
              {this.renderBody()}
            </card>
          </CardBody>
        </Card>
      </div>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
      data: state.general.selectedGraph,
      nodePro:state.general.selectedGraph && state.general.selectedGraph.data.nodePro,
      edgePro:state.general.selectedGraph && state.general.selectedGraph.data.edgePro
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
      handleNodeClick:(node)=>dispatch(generalAc.handleNodeClick(node)),
      nodeQuery:(str)=>dispatch(generalAc.nodeQuery(str))
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(MainPane));
