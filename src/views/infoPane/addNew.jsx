import React from "react";
import { connect } from 'react-redux';

// @material-ui/core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Button from "components/CustomButtons/Button.jsx";

import { FormattedMessage } from 'react-intl';

import styles from "resources/style/components/sidebarStyle.jsx";

class AddNew extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.postData = {};
  }

  handelValueChange(pro,e){
    this.postData[pro] =  e.target.value;
  }

  onConnect(){
    if (this.props.handleConnect) {
      this.props.handleConnect(this.postData)
    }
  }

  render(){
    const { classes } = this.props;
    return (
      <Drawer
        anchor="left"
        variant="permanent"
        open={true}
        classes={{
           paper: classes.drawerPaper//this.props.open?classes.drawerPaper:classes.drawerPaperHiden
        }}
        onClose={this.props.handleDrawerToggle}
        ModalProps={{
          keepMounted: true // Better open performance on mobile.
        }}
      >
        <div className={classes.sidebarWrapper}>
          <Paper className={classes.addContainerHeader} elevation={1}>
           <Typography variant="h5" component="h3">
            Add New Graph
           </Typography>
         </Paper>
          <Grid container className={classes.addContainer}>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Graph Name"
                id="graph"
                margin="normal"
                onChange={this.handelValueChange.bind(this,"graph")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Host"
                id="host"
                margin="normal"
                onChange={this.handelValueChange.bind(this,"host")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Port"
                id="port"
                margin="normal"
                onChange={this.handelValueChange.bind(this,"port")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Protocol"
                id="protocol"
                margin="normal"
                onChange={this.handelValueChange.bind(this,"protocol")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="User Name"
                id="username"
                margin="normal"
                onChange={this.handelValueChange.bind(this,"username")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} className={classes.addContainerItem}>
              <TextField
                label="Pass Word"
                id="password"
                margin="normal"
                onChange={this.handelValueChange.bind(this,"password")}
                formControlProps={{
                  fullWidth: true
                }}
              />
            </Grid>
            </Grid>
            <Button color="blue" disableRipple className={classes.addContainerButton} onClick={this.onConnect.bind(this)}><FormattedMessage id='BTN_CONNECT'/></Button>
        </div>
      </Drawer>

    );
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(AddNew));
