import React from "react";
import { connect } from 'react-redux';
import ColorPicker from 'react-color-picker';
import ss from 'react-color-picker/index.css';
import { SketchPicker,MaterialPicker   } from 'react-color';
// @material-ui/core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Button from "components/CustomButtons/Button.jsx";

import { FormattedMessage } from 'react-intl';

import styles from "resources/style/components/sidebarStyle.jsx";

class GraphInfo extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      nodeLabeChoose: Object.keys(props.nodePro)[0]
    };
  }

  onNodeLabelClick(name){
    this.setState({
      nodeLabeChoose:name
    })
  }

  render(){
    const { classes, nodePro, edgePro} = this.props;
    return (
      <Drawer
        anchor="left"
        variant="permanent"
        open={true}
        classes={{
           paper: classes.drawerPaper//this.props.open?classes.drawerPaper:classes.drawerPaperHiden
        }}
        onClose={this.props.handleDrawerToggle}
        ModalProps={{
          keepMounted: true // Better open performance on mobile.
        }}
      >
        <div className={classes.sidebarWrapper}>
          <Paper className={classes.addContainerHeader} elevation={1}>
           <Typography variant="h5" component="h3">
            Graph Detail
           </Typography>
         </Paper>
         <div className={classes.nodeContent}>
           <div className={classes.nodeTitle}>
             <FormattedMessage id="TITLE_NODE"></FormattedMessage>
           </div>
           <div className={classes.nodeContainer} >
             {nodePro && Object.keys(nodePro).map((p,i)=>{
               return (
                 <div className={p !== this.state.nodeLabeChoose? classes.nodeLabel :classes.nodeLableSelect} onClick={this.onNodeLabelClick.bind(this,p)}>{p +'('+nodePro[p].count+')'}</div>
               )
             })}
           </div>
         </div>
         <div className={classes.edgeContent}>
           <div className={classes.edgeTitle}>
             <FormattedMessage id="TITLE_EDGE"></FormattedMessage>
           </div>
           <div className={classes.edgeContainer}>
             {edgePro && Object.keys(edgePro).map((p,i)=>{
               return (
                 <div className={classes.nodeLabel}>{p +'('+edgePro[p].count+')'}</div>
               )
             })}
           </div>
         </div>
         <div className={classes.proContent}>
           <div className={classes.proTitle}>
             <FormattedMessage id="TITLE_PROPERTY"></FormattedMessage>
           </div>
           <div className={classes.edgeContainer}>
             {nodePro && nodePro[this.state.nodeLabeChoose] && nodePro[this.state.nodeLabeChoose].properties.map((p,i)=>{
               return (
                 <div className={classes.nodeLabel}>{p}</div>
               )
             })}
           </div>
         </div>
        </div>
      </Drawer>

    );
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(GraphInfo));
