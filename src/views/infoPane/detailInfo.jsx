import React from "react";
import { connect } from 'react-redux';
import ColorPicker from 'react-color-picker';
import ss from 'react-color-picker/index.css';
import { SketchPicker,MaterialPicker   } from 'react-color';
// @material-ui/core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
import Drawer from "@material-ui/core/Drawer";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Button from "components/CustomButtons/Button.jsx";

import { FormattedMessage } from 'react-intl';

import styles from "resources/style/components/sidebarStyle.jsx";

class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "",
      displayColor:false
    };
  }
  buildText(item){
    const { classes } = this.props;
    return (
      <TextField
        style={{marginBottom:0}}
       label={item[0]["@value"].label}
       type={"text"}
       formControlProps={{
         fullWidth: true
       }}
       disabled={true}
       value={item[0]["@value"].value["@value"]?new Date(parseInt(item[0]["@value"].value["@value"])).toLocaleString():item[0]["@value"].value }
       className={classes.textField, classes.textWith}
       InputLabelProps={{
         shrink: true,
       }}
       margin="normal" />
    )
  };

  onClick(color){
    this.setState({
      displayColor: !this.state.displayColor
    });
    if(this.state.color !== "" && this.props.handleColorChange){
      this.setState({
        color: ""
      });
      this.props.handleColorChange(this.state.color);
    }
  }

  handleColorChoose(color){
    this.setState({
      color:color.hex
    })
  }

  render(){
    const { classes } = this.props;
    return (
      <Drawer
        anchor="right"
        variant="temporary"
        open={this.props.open}
        classes={{
          paper: this.props.open?classes.drawerPaperInfo:classes.drawerPaperHiden
        }}
        onClose={this.props.handleDrawerToggle}
        ModalProps={{
          keepMounted: true // Better open performance on mobile.
        }}
      >
        <div className={classes.sidebarWrapper}>

         <div style={{padding:"0 10px",display:"flex"}}>
            <Button onClick={this.onClick.bind(this)} style={{width:"200px",textTransform:"none"}}>{this.state.displayColor?<FormattedMessage id='BTN_APPLY_COLOR'/>:<FormattedMessage id='BTN_CHANGE_COLOR'/>}</Button>
         </div>
         {!this.state.displayColor? null :(
             <div style={{padding:"0 10px"}}>
               <SketchPicker onChangeComplete={this.handleColorChoose.bind(this)} color={this.state.color} disableAlpha ={true}></SketchPicker >
             </div>
           )
         }
         <Grid container className={classes.addContainer}>
           {
            this.props.data.property? Object.keys(this.props.data.property).map((i,key)=>this.buildText(this.props.data.property[i])):null
           }
         </Grid>
        </div>
      </Drawer>

    );
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    }
};

const mapDispatchToProps = (dispatch)=>{
    return {
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(Detail));
