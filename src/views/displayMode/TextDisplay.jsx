import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Icon from '@material-ui/core/Icon';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import { FormattedMessage } from 'react-intl';
// core components
import tableStyle from "resources/style/components/tableStyle";

class TextDisplay extends Component {
  constructor(props, context) {
    super(props, context);
  }


  render(){
    const { classes, graphData } = this.props;
    return (
      <div>
        <span>
          Hello World!
        </span>
      </div>
    )
  }
}

export default withStyles(tableStyle)(TextDisplay);
