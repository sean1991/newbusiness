import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Icon from '@material-ui/core/Icon';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import { FormattedMessage } from 'react-intl';
// core components
import tableStyle from "resources/style/components/tableStyle";

class TableDis extends Component {
  constructor(props, context) {
    super(props, context);
  }

  formatDate(str){
    return str["@value"]?new Date(parseInt(str["@value"])).toLocaleString():str;
  }

  render(){
    const { classes, nodePro } = this.props;
    return (
      <Table className={classes.table}>
        <TableHead style={{background: nodePro && nodePro.color }}>
          <TableRow>
            {nodePro && nodePro.properties.map((prop, key) => {
              return (
                <TableCell
                  className={classes.tableCell + " " + classes.tableHeadCell}
                  key={key} >
                  {prop}
                </TableCell>
              )
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {nodePro && nodePro.data.map((prop, key) => {
            return (
              <TableRow className={classes.tableRow}>
                {nodePro && nodePro.properties.map((p, k) => {
                  return (
                    <TableCell
                      key={k} >
                      <div className={classes.tableCell + " " + classes.tableHeadCell}>
                        <span className={classes.tableCellText}>
                          {prop.property[p] ? this.formatDate(prop.property[p][0]["@value"].value) :""}
                        </span>
                      </div>
                    </TableCell>
                  );
                })}
              </TableRow>
            )
          })}

        </TableBody>
      </Table>
    )
  }
}

export default withStyles(tableStyle)(TableDis);
