const deepClone=(obj)=>{
  if(!obj){
    return null;
  }
  return JSON.parse(JSON.stringify(obj));
}

const parseData=(data)=>{
  let nodeData = data && data.result && data.result.data["@value"].length > 1 && data.result.data["@value"][0]["@value"];
  let relationData = data && data.result && data.result.data["@value"].length > 1 && data.result.data["@value"][1]["@value"];
  let nodes = [];
  let edges = [];
  let nodePro = {};
  let edgePro = {};

  for (let i = 0; i < nodeData.length; i++) {

    nodes.push({
      id: nodeData[i]["@value"].id["@value"],
      name: nodeData[i]["@value"].label,
      property: nodeData[i]["@value"].properties
    });

    if(!nodePro[nodeData[i]["@value"].label]){
      nodePro[nodeData[i]["@value"].label] = {
        color:"#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6),
        count:0,
        data:[],
        properties:[]
      }

      for (let pro in nodeData[i]["@value"].properties) {
        nodePro[nodeData[i]["@value"].label].properties.push(pro)
      }
    }

    nodePro[nodeData[i]["@value"].label].data.push({
      id: nodeData[i]["@value"].id["@value"],
      name: nodeData[i]["@value"].label,
      property: nodeData[i]["@value"].properties
    })

    nodePro[nodeData[i]["@value"].label].count++;

  }

  for (let i = 0; i < relationData.length; i++) {

    edges.push({
      id: relationData[i]["@value"].id["@value"],
      source: relationData[i]["@value"].inV["@value"],
      target: relationData[i]["@value"].outV["@value"],
      tag: relationData[i]["@value"].label,
    });

    if(!edgePro[relationData[i]["@value"].label]){
      edgePro[relationData[i]["@value"].label] = {
        color:"#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6),
        count:0
      }
    }

    edgePro[relationData[i]["@value"].label].count++;

  }

  return {
    nodes:nodes,
    nodePro:nodePro,
    edges:edges,
    edgePro:edgePro
  }

}

export default {
  deepClone,
  parseData
}
