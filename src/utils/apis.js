import oData from "data.js";
import apiConfig from "utils/apiConfig";
import util from 'utils/util';
import 'whatwg-fetch';

const createGraph = (data)=>{
  return fetch( apiConfig.GET_API(apiConfig.APIS.CREATE), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(res=>res.json());
}

const testConnect = (data)=>{
  return new Promise((resolve,reject)=>{
    resolve({
      "name":data.graph,
      "data":util.parseData(oData),
      "selected":false
    })
  })
}

const nodeQuery = (graph,str)=>{
  return new Promise((resolve,reject)=>{
    resolve(util.parseData(oData))
  })
}

export default {
  createGraph,
  testConnect,
  nodeQuery
}
