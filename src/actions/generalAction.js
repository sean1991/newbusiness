import  apis  from 'utils/apis.js';

function handleNodeClick(node){
  return (dispatch, getState) => {
    dispatch({type:"NODE_DETAIL_SHOW",payload:{showDetail:true}});
    dispatch({type:"NODE_SELECTED",payload:{node:node}});
  }
}

function handleColorChange(color){
  return{
    type: "NODE_COLOR_CHANGE",
    payload:color
  }
}

function handleShowDetail(){
  return{
    type: "NODE_DETAIL_SHOW",
    payload:{showDetail:null}
  }
}

function graphItemSelect(item){
  return{
    type: "GRAPH_ITEM_SELECT",
    payload:{item:item}
  }
}

function handleConnect(postData){
  return (dispatch, getState) => {
    apis.testConnect(postData).then((data)=>{
      dispatch({type:"CREATE_NEW_GRAPH", payload:data});
      dispatch({type:"GRAPH_ITEM_SELECT", payload:{item:data}});
    })
  };
}

function nodeQuery(str){
  return (dispatch, getState) => {
    apis.nodeQuery(str).then((data)=>{
      dispatch({type:"UPDATE_GRAPH", payload:data});
    })
  };
}

export default {
  handleNodeClick,
  handleColorChange,
  handleShowDetail,
  graphItemSelect,
  handleConnect,
  nodeQuery
}
