import data from "data.js";
import util from 'utils/util';

const graphData = util.parseData(data);

const initialState = {
  "selectedNode":{},
  "selectedGraph":{
    "name":"default",
    "data":graphData
  },
  "addSelect":false,
  "showDetail":false,
  "graphList":[{
    "name":"default",
    "data":graphData,
    "selected":true
  }]
};

export default function(state=initialState, action) {
  switch (action.type) {

    case "NODE_DETAIL_SHOW":{
      return {
        ...state,
        showDetail:!state.showDetail
      }
    }

    case "NODE_SELECTED":{
      const nodeData = state.selectedGraph.data.nodes;
      const selectedNode = nodeData.filter(item=> item.id === action.payload.node.id)[0];
      return {
        ...state,
        selectedNode: selectedNode
      }
    }

    case "NODE_COLOR_CHANGE":{
      const selectedGraph = util.deepClone(state.selectedGraph);
      selectedGraph.data.nodePro[state.selectedNode.name].color = action.payload;
      return {
        ...state,
        selectedGraph: selectedGraph
      }
    }

    case "GRAPH_ITEM_SELECT":{
      const oItem = action.payload.item;
      let changed = true;
      let selectedGraph = null;
      const graphList = state.graphList.map((item)=>{
        if(oItem && item.name == oItem.name){
          changed = item.selected ? false: true;
          item.selected = true;
          selectedGraph = {};
          selectedGraph.name = item.name;
          selectedGraph.data = item.data;
        }else{
          item.selected = false;
        }
        return item;
      })

      if (changed) {
        return {
          ...state,
          graphList: graphList,
          selectedGraph: selectedGraph,
          addSelect: oItem? false: true
        }
      }else{
        return state;
      }
    }

    case "CREATE_NEW_GRAPH":{
      const graphList =  state.graphList;
      graphList.push(action.payload);
      return {
        ...state,
        graphList: graphList
      }
    }

    case "UPDATE_GRAPH":{
      const selectedGraph =  state.selectedGraph;
      selectedGraph.data = action.payload;

      return {
        ...state,
        selectedGraph: selectedGraph,
        graphList: state.graphList.map((item,key)=>{
          if(item.name === selectedGraph.name){
            item.data = selectedGraph.data;
            return item
          }else{
            return item
          }})
      }
    }
    default:
      return state;
  }
}
