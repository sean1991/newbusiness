/* eslint-disable */
import React from "react";
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
import { FormattedMessage } from 'react-intl';
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from "@material-ui/core/IconButton";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Icon from '@material-ui/core/Icon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from "@material-ui/icons/Menu";
// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import Sidebar from "components/Sidebar/Sidebar.jsx";
import Message from 'components/message/message';
import HomeList from 'views/layout/HomeList';
import AddNew from 'views/infoPane/addNew';
import DisplayGraphInfo from 'views/infoPane/displayGraphInfo';
import DetailInfo from 'views/infoPane/detailInfo';
import MainPane from 'views/mainPane/main';

import routeRules from "routes/route.jsx";

import dashboardStyle from "resources/style/layouts/dashboardStyle.jsx";

import generalAc from "actions/generalAction.js";

const switchRoutes = (
  <Switch>
    {routeRules.routePath.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      return <Route path={prop.path} component={prop.component} key={key} />;
    })}
  </Switch>
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileOpen: true,
      infoOpen:false,
      mainClass:"mainPanel",
      collapseStatus:{},
    };
    this.resizeFunction = this.resizeFunction.bind(this);
  }
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
    //  this.setState({mainClass:!this.state.mobileOpen?"mainPanel":"mainPanelFull"});
  };

  handleInfoDrawerToggle = () => {
    this.setState({ infoOpen: !this.state.infoOpen });
    // this.setState({mainClass:!this.state.mobileOpen?"mainPanel":"mainPanelFull"});
  };

  handlShowCollapse(name){
    let tmpCollapseStatus = {...this.state.collapseStatus};
    if(tmpCollapseStatus[name] == undefined){
      tmpCollapseStatus[name] = true;
    }else{
      tmpCollapseStatus[name] = !tmpCollapseStatus[name];
    }

    this.setState({ collapseStatus: tmpCollapseStatus});
  };

  resizeFunction() {
    // if (window.innerWidth >= 960) {
    //   this.setState({ mobileOpen: false });
    // }
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      const ps = new PerfectScrollbar(this.refs.mainPanel);
    }
    window.addEventListener("resize", this.resizeFunction);
  }
  componentDidUpdate(e) {
    if (e.history.location.pathname !== e.location.pathname) {
      this.refs.mainPanel.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeFunction);
  }
  // <IconButton
  //   className={classes.showDraver}
  //   color="inherit"
  //   aria-label="open drawer"
  //   onClick={this.handleDrawerToggle}
  // >
  //   <Menu />
  // </IconButton>
  render() {
    Message.defaultProps  = this.props;
    const { classes, ...rest } = this.props;
    return (
      <div className={classes.wrapper}>
        <HomeList listData={this.props.graphList} itemSelect={this.props.itemSelect} addSelect={this.props.addSelect}></HomeList>
        {
          this.props.selectGraphL.length > 0 ? (
            <DisplayGraphInfo
              handleDrawerToggle={this.handleDrawerToggle}
              collapseStatus={this.state.collapseStatus}
              handlShowCollapse={this.handlShowCollapse.bind(this)}
              open={this.state.mobileOpen}
              nodePro={this.props.nodePro}
              edgePro={this.props.edgePro}
              color="blue"
              {...rest}
            />
          ) : (
            <AddNew
              handleDrawerToggle={this.handleDrawerToggle}
              collapseStatus={this.state.collapseStatus}
              handlShowCollapse={this.handlShowCollapse.bind(this)}
              open={this.state.mobileOpen}
              color="blue"
              handleConnect={this.props.handleConnect}
              {...rest}
            />
          )
        }

      <div className={classes[this.state.mainClass]}  ref="mainPanel">
          <div className={classes.content}>
            <div className={classes.container}>
              <MainPane ></MainPane>
            </div>
          </div>
        </div>
        {
          this.props.busy === true?(
            <div id="busy" className={classes.busyContent}>
              <CircularProgress className={classes.progress} size={150} />
            </div>
          ): null
        }
        <DetailInfo
                  handleDrawerToggle={this.handleInfoDrawerToggle}
                  open={this.props.showDetail}
                  handleDrawerToggle={this.props.handleShowDetail}
                  data={this.props.selectedNode}
                  color="blue"
                  handleColorChange={this.props.handleColorChange}
                  {...rest}></DetailInfo>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
  return {
      busy: state.general.busy,
      MessageOpen: state.general.MessageOpen,
      message:state.general.message,
      msgType : state.general.msgType,
      selectedNode:state.general.selectedNode,
      showDetail:state.general.showDetail,
      graphList:state.general.graphList,
      selectGraphL: state.general.graphList.filter(item=>item.selected ? item : null),
      nodePro:state.general.selectedGraph && state.general.selectedGraph.data.nodePro,
      edgePro:state.general.selectedGraph && state.general.selectedGraph.data.edgePro,
      addSelect:state.general.addSelect
      // message:state.general.message
    }
};

const mapDispatchToProps = (dispatch)=>{

    return {
      handleMessageClose:()=>dispatch(generalAc.messageStateChange({MessageOpen:false})),
      handleShowDetail:()=>dispatch(generalAc.handleShowDetail()),
      handleColorChange:(color)=>dispatch(generalAc.handleColorChange(color)),
      itemSelect:(item)=>dispatch(generalAc.graphItemSelect(item)),
      handleConnect:(data)=>dispatch(generalAc.handleConnect(data))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(dashboardStyle)(App));
