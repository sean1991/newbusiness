* test MonthlyRebateRule with curl
* use -k (or --insecure) option for curl!
CREATE HEAD TOGETHER WITH ITEMS:
```ssh
curl -i -X POST -H "Content-Type: application/json" -d '{"header": {"monthlyRebateRuleNumber": "M-2018-010","description": "2018 10月返规则与item同时创建","validFromDate": "2018-02-01 00:00:00","validToDate": "2018-02-28 00:00:00","memo":"备注"},"monthlyrebateruleitem":[{"rebateProductGroupNumber": "RPG-0001","baseUnitPrice":"100","baseUnit":"KG","priceDifference":"10","rebatePrice1st":"5","rebatePrice2nd":"3","rebateCurrency":"RMB"}, { "rebateProductGroupNumber": "RPG-0002","baseUnitPrice":"200","baseUnit":"KG","priceDifference":"20","rebatePrice1st":"6","rebatePrice2nd":"3","rebateCurrency":"RMB"}]}' localhost:3000/api/CreateMonthlyRebateRule
```
CREATE ITEM
```ssh
curl -ik -X POST -H "Content-Type: application/json" -d '{ "monthlyRebateRuleNumber": "M-2018-001","rebateProductGroupNumber": "RPG-0001","baseUnitPrice":"100","baseUnit":"KG","priceDifference":"10","rebatePrice1st":"5","rebatePrice2nd":"3","rebateCurrency":"RMB"}' http://localhost:3000/api/CreateMontlyRebateRuleItems
```
READ
```ssh
curl -ik -H "Accept: application/json" http://localhost:3000/api/GetMonthlyRebateRule/M-2018-001
```
LIST
```ssh
curl -ik -H "Accept: application/json" http://localhost:3000/api/GetMonthlyRebateRule
```
UPDATE MonthlyRebateRule M-2018-002
```ssh
curl -ik -X PUT -H "Content-Type: application/json" -d '{ "description": "2018年2月返规则002","validFromDate": "2018-02-01 00:00:00","validToDate": "2018-02-28 00:00:00","memo":"备注更新"}' http://localhost:3000/api/UpdateMonthlyRebateRuleHeader/M-2018-002
```
UPDATE ITEM  with rebateProductGroupNumber "RPG-0001" from MonthlyRebateRule "M-2018-001"
```ssh
curl -ik -X PUT -H "Content-Type: application/json" -d '{ "rebateProductGroupNumber": "RPG-0001","baseUnitPrice":"120","baseUnit":"KG","priceDifference":"16","rebatePrice1st":"5","rebatePrice2nd":"2","rebateCurrency":"RMB"}' http://localhost:3000/api/UpdateMonthlyRebateRuleItems/M-2018-001
```
DELETE MonthlyRebateRule "M-2018-004"
```ssh
curl -ik -X DELETE -H "Content-Type: application/json" http://localhost:3000/api/DeleteMonthlyRebateRuleHeader/M-2018-004
```

DELETE ITEM with rebateProductGroupNumber "RPG-0001" from MonthlyRebateRule "M-2018-004"
```ssh
curl -ik -X DELETE -H "Content-Type: application/json"  -d '{ "rebateProductGroupNumber": "RPG-0001"}' http://localhost:3000/api/DeleteMonthlyRebateRuleItems/M-2018-004
```

* test yearlyRebateRule with curl:
create yearlyrebaterule

```ssh
curl -ik -X POST -H "Content-Type: application/json" -d '{ "yearlyRebateRuleNumber": "Y-2018-002","description": "2018年返利规则002","validFromDate": "2018-01-01 00:00:00","validToDate": "2018-12-31 00:00:00","rebateProductGroupNumber":"RPG-0099","actualSaleAchievement":"0.7","additionalRebateRate":"0.005", "memo":"备注"}' http://localhost:3000/api/CreateYearlyRebateRule
```

 UPDATE YearlyRebateRule Y-2018-002

```ssh
curl -ik -X PUT -H "Content-Type: application/json" -d '{ "description": "2018年返利规则002upd","validFromDate": "2018-01-01 00:00:01","validToDate": "2018-12-31 00:00:00","rebateProductGroupNumber":"RPG-0099","actualSaleAchievement":"0.7","additionalRebateRate":"0.005", "memo":"备注"}' http://localhost:3000/api/UpdateYearlyRebateRuleHeader/Y-2018-002
```

CREATE ITEM

```ssh
curl -ik -X POST -H "Content-Type: application/json" -d '{ "yearlyRebateRuleNumber": "Y-2018-002","itemNumber": "10","rangeFrom":"50","rangeTo":"200","unit":"万元","rebateRate":"0.01"}' http://localhost:3000/api/CreateYearlyRebateRuleItems

curl -ik -X POST -H "Content-Type: application/json" -d '{ "yearlyRebateRuleNumber": "Y-2018-002","itemNumber": "20","rangeFrom":"200","rangeTo":"500","unit":"万元","rebateRate":"0.02"}' http://localhost:3000/api/CreateYearlyRebateRuleItems

curl -ik -X POST -H "Content-Type: application/json" -d '{ "yearlyRebateRuleNumber": "Y-2018-002","itemNumber": "30","rangeFrom":"500","rangeTo":"1000","unit":"万元","rebateRate":"0.03"}' http://localhost:3000/api/CreateYearlyRebateRuleItems
```

READ

```ssh
curl -ik -H "Accept: application/json" http://localhost:3000/api/GetYearlyRebateRule/Y-2018-002
```

DELETE YearlyRebateRule "Y-2018-004"

```ssh
curl -ik -X DELETE -H "Content-Type: application/json" http://localhost:3000/api/DeleteYearlyRebateRuleHeader/Y-2018-004
```

DELETE ITEM with itemNumber "10" from YearlyRebateRule "Y-2018-004"

```ssh
curl -ik -X DELETE -H "Content-Type: application/json"  -d '{ "itemNumber": "10"}' http://localhost:3000/api/DeleteYearlyRebateRuleItems/Y-2018-004
```
* test GetMonthlyRebate
http://localhost:3000/api/GetMonthlyRebate/2018-02/101/sjlh
* test GetYearlyRebate
http://localhost:3000/api/GetYearlyRebate/2018-02/101/sjlh
* test GetRebateBalance
http://localhost:3000/api/GetRebateBalance/101/sjhl
* test GetRebateConsumptionDetails
http://localhost:3000/api/GetRebateConsumptionDetails/101/sjhl

* test CustomerRebateStrategy
http://localhost:3000/api/GetCustomerRebateStrategy
http://localhost:3000/api/GetCustomerRebateStrategy/sjlh

* create/update CustomerRebateStrategy
curl -ik -X POST -H "Content-Type: application/json" -d '{ "customerNumber": "sjlh", "validFromDate": "2017-01-01","validToDate": "2018-12-31","monthlyRebateRuleNumber": "M-2018-001", "monthlyRebateRuleDescription":"月度返利规则","yearlyRebateRuleNumber": "Y-2018-002","yearlyRebateRuleDescription":"年度返利规则002","memo":"测试分配"}' http://localhost:3000/api/UpdateCustomerRebateStrategy/sjlh

* test with ARC:
![api_test](api_test.png)

* Sample return data:
http://localhost:3000/api/GetMonthlyRebateRuleHeader/M-2018-001

{
    "mRRid":1003,
    "monthlyRebateRuleNumber":"M-2018-001",
    "description":"2018月返规则001","validFromDate":"2017-12-31T16:00:00.000Z","validToDate":"2018-01-30T16:00:00.000Z",
    "memo":"备注",
    "monthlyrebateruleitem":[
        {
            "mIRid":1,
            "monthlyRebateRuleNumber":"M-2018-001",
            "rebateProductGroupNumber":"RPG-0001",
            "baseUnitPrice":100,
            "baseUnit":"KG",
            "priceDifference":10,
            "rebatePrice1st":5,
            "rebatePrice2nd":3,
            "rebateCurrency":"RMB"
        },
        {
            "mIRid":2,
            "monthlyRebateRuleNumber":"M-2018-001",
            "rebateProductGroupNumber":"RPG-0002",
            "baseUnitPrice":200,
            "baseUnit":"KG",
            "priceDifference":20,
            "rebatePrice1st":6,
            "rebatePrice2nd":3,
            "rebateCurrency":"RMB"
        }
    ]
}